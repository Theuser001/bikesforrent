﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BikesForRent.Services
{
    public class UtilsService : IUtilsService
    {
        public UtilsService(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
        }

        public IWebHostEnvironment WebHostEnvironment { get; }
        public bool IsEmpty<T>(IEnumerable<T> data)
        {
            if (data == null)
            {
                return true;
            }
            else
            {
                return !data.Any();
            }


        }
    }
}
