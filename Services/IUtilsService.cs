﻿using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;

namespace BikesForRent.Services
{
    public interface IUtilsService
    {
        IWebHostEnvironment WebHostEnvironment { get; }

        bool IsEmpty<T>(IEnumerable<T> data);
    }
}