﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikesForRent.Data;
using BikesForRent.Models;

namespace BikesForRent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BikeTypesController : ControllerBase
    {
        private readonly BikesForRentContext _context;

        public BikeTypesController(BikesForRentContext context)
        {
            _context = context;
        }
        public bool BikeTypesExists(int id)
        {
            return _context.BikeTypes.Any(e => e.Id == id);
        }

        // GET: api/BikeTypes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BikeTypes>>> GetBikeTypes()
        {
            return await _context.BikeTypes.ToListAsync();
        }

        // GET: api/BikeTypes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BikeTypes>> GetBikeTypes(int id)
        {
            var bikeTypes = await _context.BikeTypes.FindAsync(id);

            if (bikeTypes == null)
            {
                return NotFound();
            }

            return bikeTypes;
        }

        // PUT: api/BikeTypes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBikeTypes(int id, BikeTypes bikeTypes)
        {
            if (id != bikeTypes.Id)
            {
                return BadRequest();
            }

            _context.Entry(bikeTypes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BikeTypesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BikeTypes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<BikeTypes>> PostBikeTypes(BikeTypes bikeTypes)
        {
            _context.BikeTypes.Add(bikeTypes);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBikeTypes", new { id = bikeTypes.Id }, bikeTypes);
        }

        // DELETE: api/BikeTypes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<BikeTypes>> DeleteBikeTypes(int id)
        {
            var bikeTypes = await _context.BikeTypes.FindAsync(id);
            if (bikeTypes == null)
            {
                return NotFound();
            }

            _context.BikeTypes.Remove(bikeTypes);
            await _context.SaveChangesAsync();

            return bikeTypes;
        }

        //Delete all biketypes:
        [HttpDelete]
        public async Task<ActionResult<IEnumerable<BikeTypes>>> DeleteBikeTypes()
        {
            var results = await _context.BikeTypes.ToListAsync();

            foreach (var type in results)
            {
                _context.BikeTypes.Remove(type);
            }
            await _context.SaveChangesAsync();
            return results;
        }
    }
}
