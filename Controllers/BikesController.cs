﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikesForRent.Data;
using BikesForRent.Models;

namespace BikesForRent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BikesController : ControllerBase
    {
        private readonly BikesForRentContext _context;

        public BikesController(BikesForRentContext context)
        {
            _context = context;
        }

        public bool BikesExists(int id)
        {
            return _context.Bikes.Any(e => e.Id == id);
        }

        // GET: api/Bikes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Bikes>>> GetBikes()
        {
            return await _context.Bikes.ToListAsync();
        }

        // GET: api/Bikes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Bikes>> GetBikes(int id)
        {
            var bikes = await _context.Bikes.FindAsync(id);

            if (bikes == null)
            {
                return NotFound();
            }

            return bikes;
        }

        // PUT: api/Bikes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBikes(int id, Bikes bikes)
        {
            if (id != bikes.Id)
            {
                return BadRequest();
            }

            _context.Entry(bikes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BikesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Bikes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Bikes>> PostBikes(Bikes bikes)
        {
            _context.Bikes.Add(bikes);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBikes", new { id = bikes.Id }, bikes);
        }

        // DELETE: api/Bikes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Bikes>> DeleteBikes(int id)
        {
            var bikes = await _context.Bikes.FindAsync(id);
            if (bikes == null)
            {
                return NotFound();
            }

            _context.Bikes.Remove(bikes);
            await _context.SaveChangesAsync();

            return bikes;
        }

        //Order bikes based on their brand name:
        [HttpGet("OrderByBrand")]
        public async Task<ActionResult<IEnumerable<Bikes>>> OrderByBrand()
        {
            var results = await _context.Bikes.OrderBy(bike => bike.BikeType.BrandName).ToListAsync();

            return results;
        }

        //Order bikes based on their price:
        [HttpGet("OrderByPrice")]
        public async Task<ActionResult<IEnumerable<Bikes>>> OrderByPrice()
        {
            var results = await _context.Bikes.OrderBy(b => b.Price).ToListAsync();

            return results;
        }

        //Filter by brand:
        [HttpGet("FilterByBrand/{brandname}")]
        public async Task<ActionResult<IEnumerable<Bikes>>> FilterBrand(string brandname)
        {
            var results = await _context.Bikes.Where(b => b.BikeType.BrandName == brandname).ToListAsync();
            //Console.WriteLine(results);
            return results;
        }

        //Filter by model:
        [HttpGet("FilterByModel/{brandname}/{modelname}")]
        public async Task<ActionResult<IEnumerable<Bikes>>> FilterModel(string brandname, string modelname)
        {
            //var bikes = _context.Bikes.Where(b => b.BikeType.BrandName == brandname);
            //Console.WriteLine(brand);
            //var results = await bikes.Where(b => (b.BikeType.ModelName) == modelname)).ToListAsync();
            var results = await _context.Bikes.Where(b => b.BikeType.BrandName == brandname && b.BikeType.ModelName == modelname).ToListAsync();
            return results;
        }


        //Get the bikes of a specific price range:
        [HttpGet("FilterByPrice/min={min}-max={max}")]
        public async Task<ActionResult<IEnumerable<Bikes>>> FilterPrice(long min, long max)
        {
            var results = await _context.Bikes.Where(b => (b.Price >= min && b.Price <= max)).ToListAsync();

            return results;
        }

        //Delete all bikes:
        [HttpDelete]
        public async Task<ActionResult<IEnumerable<Bikes>>> DeleteBikes()
        {
            var results = await _context.Bikes.ToListAsync();

            foreach (var bike in results)
            {
                _context.Bikes.Remove(bike);
            }
            await _context.SaveChangesAsync();
            return results;
        }
    }
}
