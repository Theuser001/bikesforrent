﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikesForRent.Data;
using BikesForRent.Models;
using BikesForRent.Services;

namespace BikesForRent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly BikesForRentContext _context;
        public IUtilsService UtilsService { get; }


        public CustomersController(BikesForRentContext context, IUtilsService utilsService)
        {
            _context = context;
            UtilsService = utilsService;
        }

        public bool CustomersExists(int id)
        {
            return _context.Customers.Any(e => e.Id == id);
        }

        // GET: api/Customers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Customers>>> GetCustomers()
        {
            return await _context.Customers.ToListAsync();
        }

        // GET: api/Customers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Customers>> GetCustomers(int id)
        {
            var customers = await _context.Customers.FindAsync(id);

            if (customers == null)
            {
                return NotFound();
            }

            return customers;
        }

        // PUT: api/Customers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustomers(int id, Customers customers)
        {
            if (id != customers.Id)
            {
                return BadRequest();
            }

            _context.Entry(customers).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Customers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Customers>> PostCustomers(Customers customers)
        {
            _context.Customers.Add(customers);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCustomers", new { id = customers.Id }, customers);
        }

        // DELETE: api/Customers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Customers>> DeleteCustomers(int id)
        {
            var customers = await _context.Customers.FindAsync(id);
            if (customers == null)
            {
                return NotFound();
            }

            _context.Customers.Remove(customers);
            await _context.SaveChangesAsync();

            return customers;
        }

        //Filer customers by username:
        //GET: api/Customers/Username/{"IAmPresto"}
        [HttpGet("Username/{username}")]
        public async Task<ActionResult<IEnumerable<Customers>>> FilterUsername(string username)
        {
            var results = await _context.Customers.Where(c => (c.Username.Contains(username))).ToListAsync();
            //if (UtilsService.IsEmpty(results))
            //{
            //    return NotFound();
            //}
            return results;
        }

        //Filter customers by account type
        //GET: api/Customers/AccType/{"user001"}
        [HttpGet("AccType/{acctype}")]
        public async Task<ActionResult<IEnumerable<Customers>>> FilterAccountType(string acctype)
        {
            var results = await _context.Customers.Where(c => (c.AccountType.TypeName.Contains(acctype))).ToListAsync();
            return results;
        }

        //Delete all customers
        //DELETE: api/customers
        [HttpDelete]
        public async Task<ActionResult<IEnumerable<Customers>>> DeleteCustomers()
        {
            var results = await _context.Customers.ToListAsync();
            foreach (var customer in results)
            {
                _context.Customers.Remove(customer);
            }
            await _context.SaveChangesAsync();
            return results;
        }
    }
}
