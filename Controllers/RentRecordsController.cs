﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikesForRent.Data;
using BikesForRent.Models;
using BikesForRent.Services;

namespace BikesForRent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RentRecordsController : ControllerBase
    {
        private readonly BikesForRentContext _context;
        public IUtilsService UtilsService { get; }
        public RentRecordsController(BikesForRentContext context, IUtilsService utilsService)
        {
            _context = context;
            UtilsService = utilsService;
        }

        public bool RentRecordsExists(int id)
        {
            return _context.RentRecords.Any(e => e.Id == id);
        }


        // GET: api/RentRecords
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RentRecords>>> GetRentRecords()
        {
            return await _context.RentRecords.ToListAsync();
        }

        // GET: api/RentRecords/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RentRecords>> GetRentRecords(int id)
        {
            var rentRecords = await _context.RentRecords.FindAsync(id);

            if (rentRecords == null)
            {
                return NotFound();
            }

            return rentRecords;
        }

        // PUT: api/RentRecords/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRentRecords(int id, RentRecords rentRecords)
        {
            if (id != rentRecords.Id)
            {
                return BadRequest();
            }

            _context.Entry(rentRecords).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RentRecordsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RentRecords
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<RentRecords>> PostRentRecords(RentRecords rentRecords)
        {
            _context.RentRecords.Add(rentRecords);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRentRecords", new { id = rentRecords.Id }, rentRecords);
        }

        // DELETE: api/RentRecords/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RentRecords>> DeleteRentRecords(int id)
        {
            var rentRecords = await _context.RentRecords.FindAsync(id);
            if (rentRecords == null)
            {
                return NotFound();
            }

            _context.RentRecords.Remove(rentRecords);
            await _context.SaveChangesAsync();

            return rentRecords;
        }
        //Search for rent records based on bike id
        //GET: api/RentRecords/bikes/{123}
        [HttpGet("bikes/{id}")]
        public async Task<ActionResult<IEnumerable<RentRecords>>> FilterBike(int bikeId)
        {
            var results = await _context.RentRecords.Where(rr => (rr.Rent.BikeId == bikeId)).ToListAsync();
            BikesController BikesController = new BikesController(_context);
            if (!BikesController.BikesExists(bikeId))
            {
                return NotFound();
            }
            return results;
        }

        //Search for rent records based on customer id
        //GET: api/RentRecords/customers/{123}
        [HttpGet("customers/{id}")]
        public async Task<ActionResult<IEnumerable<RentRecords>>> FilterCustomer(int customerId)
        {
            var results = await _context.RentRecords.Where(rr => (rr.Rent.CustomerId == customerId)).ToListAsync();
            CustomersController CustomerController = new CustomersController(_context, UtilsService);
            if (!CustomerController.CustomersExists(customerId))
            {
                return NotFound();
            }
            return results;
        }

        //Search for all rent records that starts on a date
        //GET: api/RentRecords/datestart/{datestart}
        [HttpGet("datestart/{datestart}")]
        public async Task<ActionResult<IEnumerable<RentRecords>>> FilterDateStart(DateTime dateStart)
        {
            var results = await _context.RentRecords.Where(rr => (rr.TimeStart.Date == dateStart.Date)).ToListAsync();
            return results;
        }

        //Search for rent records that ends on a date
        //GET: api/RentRecords/dateend/{dateend}
        [HttpGet("dateend/{dateend}")]
        public async Task<ActionResult<IEnumerable<RentRecords>>> FilterDateEnd(DateTime dateEnd)
        {
            var results = await _context.RentRecords.Where(rr => (rr.TimeEnd.Date == dateEnd.Date)).ToListAsync();
            return results;
        }

        //Delete all rent records
        //DELETE: api/RentRecords/delete
        [HttpDelete]
        public async Task<ActionResult<IEnumerable<RentRecords>>> DeleteRentRecords()
        {
            var results = await _context.RentRecords.ToListAsync();
            foreach (var record in results)
            {
                _context.RentRecords.Remove(record);
            }
            return results;
        }
    }
}
