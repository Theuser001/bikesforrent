﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BikesForRent.Models;

namespace BikesForRent.Data
{
    public partial class BikesForRentContext : DbContext
    {
        public BikesForRentContext()
        {

        }
        public BikesForRentContext(DbContextOptions<BikesForRentContext> options)
            : base(options)
        {
        }

        public DbSet<BikesForRent.Models.Bikes> Bikes { get; set; }

        public DbSet<BikesForRent.Models.AccountTypes> AccountTypes { get; set; }

        public DbSet<BikesForRent.Models.BikeTypes> BikeTypes { get; set; }

        public DbSet<BikesForRent.Models.Customers> Customers { get; set; }

        public DbSet<BikesForRent.Models.Rents> Rents { get; set; }

        public DbSet<BikesForRent.Models.RentRecords> RentRecords { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountTypes>(entity =>
            {
                entity.ToTable("AccountTypes");
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.DiscountRate).HasColumnName("DiscountRate");
                entity.Property(e => e.TypeName).HasColumnName("TypeName");
            });

            modelBuilder.Entity<Bikes>(entity =>
            {
                entity.ToTable("Bikes");
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.BikeTypeId).HasColumnName("BikeTypeId");
                entity.Property(e => e.Color).HasColumnName("Color");
                entity.Property(e => e.isAvailable).HasColumnName("isAvailable");
                entity.Property(e => e.Price).HasColumnName("Price");

                entity.HasOne(d => d.BikeType).WithMany(p => p.Bikes).HasForeignKey(d => d.BikeTypeId).HasConstraintName("Bikes_FK");

            });
            modelBuilder.Entity<BikeTypes>(entity =>
            {
                entity.ToTable("BikeTypes");
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.Id).HasColumnName("BrandName");
                entity.Property(e => e.ModelName).HasColumnName("ModelName");
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.ToTable("Customers");
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.AccountTypeId).HasColumnName("AccountTypeId");
                entity.Property(e => e.Balance).HasColumnName("Balance");
                entity.Property(e => e.Password).HasColumnName("Password").HasMaxLength(15).IsUnicode(false);

                entity.HasOne(d => d.AccountType).WithMany(p => p.Customers).OnDelete(DeleteBehavior.Cascade).HasConstraintName("Remts_FK1");


            });

            modelBuilder.Entity<Rents>(entity =>
            {
                entity.ToTable("Rents");
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.BikeId).HasColumnName("BikeId");
                entity.Property(e => e.CustomerId).HasColumnName("CustomerId");

                entity.HasOne(d => d.Customer).WithMany(p => p.Rents).HasForeignKey(d => d.CustomerId).HasConstraintName("Rents_FK1");
                entity.HasOne(d => d.Bike).WithMany(p => p.Rents).HasForeignKey(d => d.BikeId).HasConstraintName("Rents_FK2");

            });

            modelBuilder.Entity<RentRecords>(entity =>
            {
                entity.ToTable("RentRecords");
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.RentId).HasColumnName("RentId");
                entity.Property(e => e.TimeStart).HasColumnName("TimeStart").HasColumnType("datetime");
                entity.Property(e => e.TimeEnd).HasColumnName("TimeEnd").HasColumnType("datetime");
                entity.Property(e => e.totalPrice).HasColumnName("TotalPrice");

                entity.HasOne(d => d.Rent).WithOne(p => p.RentRecord).HasForeignKey<RentRecords>(d => d.RentId).HasConstraintName("RentRecords_FK");
            });

            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
