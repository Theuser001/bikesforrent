﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BikesForRent.Models
{
    public class AccountTypes
    {
        public AccountTypes()
        {
            Customers = new HashSet<Customers>();
        }
        public int Id { get; set; }
        public string TypeName { get; set; }
        public int DiscountRate { get; set; }
        public ICollection<Customers> Customers { get; set; }

        public override string ToString() => JsonSerializer.Serialize<AccountTypes>(this);
    }
    public class Customers
    {
        public Customers()
        {
            Rents = new HashSet<Rents>();
        }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int AccountTypeId { get; set; }
        public AccountTypes AccountType { get; set; }
        public long Balance { get; set; }
        public ICollection<Rents> Rents { get; set; } = new HashSet<Rents>();
        public override string ToString() => JsonSerializer.Serialize<Customers>(this);


    }
    public class BikeTypes
    {
        public BikeTypes()
        {
            Bikes = new HashSet<Bikes>();
        }
        public int Id { get; set; }
        public string BrandName { get; set; }
        public string ModelName { get; set; }
        public ICollection<Bikes> Bikes { get; set; }

        public override string ToString() => JsonSerializer.Serialize<BikeTypes>(this);
    }
    public class Bikes
    {
        public Bikes()
        {
            Rents = new HashSet<Rents>();
            isAvailable = true;
        }
        public int Id { get; set; }
        public int BikeTypeId { get; set; }
        public BikeTypes BikeType { get; set; }
        public string Color { get; set; }
        public long Price { get; set; }
        public bool isAvailable { get; set; }
        public ICollection<Rents> Rents { get; set; }

        public override string ToString() => JsonSerializer.Serialize<Bikes>(this);
      

    }
    public class Rents
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public Customers Customer { get; set; }
        public int BikeId{ get; set; }
        public Bikes Bike { get; set; }
        public RentRecords RentRecord { get; set; }

        public override string ToString() => JsonSerializer.Serialize<Rents>(this);

    }
    public class RentRecords
    {
        public int Id { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
        public long totalPrice { get; set; }
        public int RentId { get; set; }
        public Rents Rent { get; set; }

        public override string ToString() => JsonSerializer.Serialize<RentRecords>(this);

    }
}


